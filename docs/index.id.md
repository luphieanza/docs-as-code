# Welcome to Warung Pintar Internal TechDocs

Untuk dokumentasi selengkapnya kunjungi [mkdocs.org](https://www.mkdocs.org).

![Warung Pintar LOGO](img/Logo_WP (Primary).svg)

## Commands

* `mkdocs new [dir-name]` - Membuat project baru.
* `mkdocs serve` - Memulai sesi live-reloading di server docs.
* `mkdocs build` - Build situs dokumentasi.
* `mkdocs -h` - Cetak pesan bantuan dan keluar.

## Project layout

    mkdocs.yml    # The configuration file.
    docs/
        index.md  # The documentation homepage.
        ...       # Other markdown pages, images and other files.
